import React, { useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import FormControl from '@mui/material/FormControl';
import Typography from '@mui/material/Typography';
import Loader from '@mui/material/CircularProgress';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import Paper from '@mui/material/Paper';
import MenuItem from '@mui/material/MenuItem';
import Lottie from 'react-lottie';
import dayjs from 'dayjs';
import {
    Chart,
    BarSeries,
    Title,
    ArgumentAxis,
    ValueAxis,
  } from '@devexpress/dx-react-chart-material-ui';
  import { Animation } from '@devexpress/dx-react-chart';

import { wash } from './animations';
import { useCountriesQuery, useLazyGetStatsQuery } from './api';

const App = () => {
    const [isPlaying, setIsPlaing] = useState(true);
    const [country, setCounty] = useState<string>(null);
    const { data: countries, isLoading, isUninitialized } = useCountriesQuery(null);
    const [getStats, statuses] = useLazyGetStatsQuery();

    const handleAnimEnds = () => {
        setIsPlaing(false);
    }

    const handleChange = (event) => {
        const nextValue = event.target.value;

        setCounty(nextValue);
    }

    useEffect(() => {
        if (country) {
            getStats(country);
        }
    }, [country]);

    if (isPlaying) {
        return (
            <Box sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column'
            }}>
                <Typography variant="h1">Не забывайте мыть руки</Typography>

                <Lottie
                    options={{
                        animationData: wash,
                    }}
                    eventListeners={[
                        {
                            eventName: 'loopComplete',
                            callback: handleAnimEnds,
                        },
                    ]}
                    height={600}
                    width={600}
                />
            </Box>
        )
    }

    if (isLoading || isUninitialized) {
        return (
            <Loader />

        )
    }

    return (
        <>
            <Box sx={{ flexGrow: 1 }}>
                <AppBar position="static">
                    <Toolbar>
                        <Typography>Ситуация по COVID-19</Typography>
                    </Toolbar>
                </AppBar>
            </Box>
            <Box sx={{
                p: 2
            }}>
                <Box sx={{ minWidth: 120 }}>
                    <FormControl fullWidth variant='standard' sx={{ m: 1, minWidth: '320px' }}>
                        <InputLabel id="demo-simple-select-label">Выберите страну</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={country}
                            label="Age"
                            onChange={handleChange}
                        >
                            {countries.map(c => (
                                <MenuItem key={c.Country} value={c.Slug}>{c.Country}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </Box>
                <Paper>
                    {statuses.isLoading && <Loader />}
                    {statuses.isSuccess && (
                        <Chart
                            data={statuses.data.map(d => ({ ...d, date: dayjs(d.Date).format('DD:MM') }))}
                        >
                            <ArgumentAxis />
                            <ValueAxis />

                            <BarSeries
                                valueField="Cases"
                                argumentField="date"
                            />
                            <Title text="Статистика по COVID-19 за последние 30 дней" />
                            <Animation />
                        </Chart>
                    )}
                </Paper>
            </Box>
        </>
    )
}

export default App;

