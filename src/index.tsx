import React from 'react';
import ReactDom from 'react-dom';
import { ApiProvider } from '@reduxjs/toolkit/query/react';

import { api } from './api';

import App from './app';

export default () => (
    <ApiProvider api={api}>
        <App/>
    </ApiProvider>
);

export const mount = (Сomponent) => {
    ReactDom.render(
        <Сomponent/>,
        document.getElementById('app')
    );

    if(module.hot) {
        module.hot.accept('./app', ()=> {
            ReactDom.render(
                <App/>,
                document.getElementById('app')
            );
        })
    }
};

export const unmount = () => {
    ReactDom.unmountComponentAtNode(document.getElementById('app'));
};

